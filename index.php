<?php
require 'animal.php';
require 'frog.php';
require 'ape.php';

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "\n"."<br>";
echo "legs : " . $sheep->legs . "\n"."<br>";
echo "cold blooded : " . $sheep->cold_blooded . "\n\n". "<br><br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "\n"."<br>";
echo "legs : " . $kodok->legs . "\n"."<br>";
echo "cold blooded : " . $kodok->cold_blooded . "\n"."<br>" ;
echo "jump : " . $kodok->jump() . "\n\n". "<br><br>";;

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "\n"."<br>";
echo "legs : " . $sungokong->legs . "\n"."<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "\n"."<br>";
echo "Yell : " . $sungokong->yell() . "\n" ."<br><br>";
?>
